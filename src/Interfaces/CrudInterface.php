<?php

interface CrudInterface
{
    public function store(Request $request): Array;
}